python3 -m venv .venv
source .venv/bin/activate
python -m pip install --upgrade pip
sudo setcap 'cap_net_raw,cap_net_admin+eip' .venv/lib/python3.8/site-packages/bluepy/bluepy-helper # this allows non-sudo users to use BLE
pip install -r requirements.txt
