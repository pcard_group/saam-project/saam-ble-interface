#!/usr/bin/env bash
#set -x

TARGET=$(head -n 1 report_server_target.txt)
echo "${TARGET}"


TIMESTAMP=$(date "+%Y_%m_%d-%H_%M_%S")
echo $TIMESTAMP
#for fn in "${arr[@]}"
#do
#    echo "$fn"
#    if test -f "$fn"; then
#        scp "${fn}" "${TARGET}/${fn}"
#        scp "${fn}" "${TARGET}/${TIMESTAMP}_${fn}"
#    fi
#done

#files to copy
#declare -a arr=("SAAM_nodes.html" "SAAM_microhub.html" "SAAM_UWB.html")

fn="SAAM_nodes.html"
echo "$fn"
if test -f "$fn"; then
    scp "${fn}" "${TARGET}/SAAM_nodes/${fn}"
    scp "${fn}" "${TARGET}/SAAM_nodes/${TIMESTAMP}_${fn}"
fi

fn="SAAM_microhub.html"
echo "$fn"
if test -f "$fn"; then
    scp "${fn}" "${TARGET}/SAAM_microhub/${fn}"
    scp "${fn}" "${TARGET}/SAAM_microhub/${TIMESTAMP}_${fn}"
fi

fn="SAAM_UWB.html"
echo "$fn"
if test -f "$fn"; then
    scp "${fn}" "${TARGET}/SAAM_UWB/${fn}"
    scp "${fn}" "${TARGET}/SAAM_UWB/${TIMESTAMP}_${fn}"
fi
