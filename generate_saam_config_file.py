#!/usr/bin/python3


from bluepy.btle import Scanner, DefaultDelegate

template = """{{
  "location": "/etc/lgtc/loc-id",
  "topic": "saam/data",
  "mqtt_on": true,
  "mqtt_timestep":10,
  "mqtt_ip": "mqtt.saam-platform.eu",
  "mqtt_port": 8883,
  "mqtt_user": "DeviceUser",
  "mqtt_pwd": "b9BpukeK",
  "devices": [
    {{
      "type": "microhub",
      "hubType":"BED",
      "mac": "{0}",
      "location": "sens_bed",
      "comment": "{1}"
    }},
    {{
      "type": "microhub",
      "hubType":"CLIP",
      "mac": "{2}",
      "location": "sens_belt",
      "comment": "{3}"
    }},
    {{
      "type": "microhub",
      "hubType":"WRIST",
      "mac":"{4}",
      "location": "sens_bracelet_left",
      "comment": "{5}"
    }}
  ]
}}"""


def minimal_scan(list_names=[], time=10, non_blocking_scan=False):
    ret_val = []
    devs = (
        Scanner()
        .withDelegate(DefaultDelegate())
        .scan(timeout=time, passive=non_blocking_scan)
    )
    if len(list_names) == 0:
        ret_val = devs
    else:
        for name in list_names:
            ret_val.extend(
                list(
                    filter(
                        lambda d: str(d.getValue(9)).lower().startswith(name.lower()),
                        devs,
                    )
                )
            )

    return list(
        map(
            lambda d: {
                "name": str(d.getValue(9)),
                "mac": str(d.addr),
                "addrType": d.addrType,
                "rssi": d.rssi,
            },
            ret_val,
        )
    )


if __name__ == "__main__":

    _filepath = "/opt/saamConfig.json"
    _t = 5

    print("scanning for {} seconds!".format(_t))
    devs = minimal_scan(time=_t)
    print("scan finished! Found {} devices".format(len(devs)))
    bed = None
    clip = None
    wrist = None
    for i in devs:
        if i["name"].startswith("BED_"):
            bed = i
        if i["name"].startswith("CLIP_"):
            clip = i
        if i["name"].startswith("SAAM_WRIST"):
            wrist = i

    if bed and clip and wrist:
        temp = template.format(
            bed["mac"],
            bed["name"],
            clip["mac"],
            clip["name"],
            wrist["mac"],
            wrist["name"],
        )

        try:
            with open(_filepath, "w") as f:
                f.write(temp)
        except Exception as e:
            print(e)

        print("\n-----------------------------------------\n")
        print(temp)
        print("\n-----------------------------------------\n")
        print(
            "{}\t{}\t{}\t{}\t{}\t{}\t".format(
                bed["name"],
                bed["mac"],
                clip["name"],
                clip["mac"],
                wrist["name"],
                wrist["mac"],
            )
        )
        print("\n-----------------------------------------\n")
    else:
        print(
            "\033[93mMissing devices!\n\nBED  : {}\n\nCLIP : {}\n\nWRIST: {}\033[0m".format(
                bed, clip, wrist
            )
        )
