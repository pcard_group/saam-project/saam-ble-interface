#!/usr/bin/env python3


def getDeploymentInfo(csv_url):
    import requests
    import csv as p_csv
    from io import StringIO

    # print(csv_url)
    res = requests.get(url=csv_url, timeout=15)
    if res.status_code != 200:
        print("Error downloading CSV!")
        exit()
    # print(res.text)
    inp_file = StringIO(res.text)
    csv = p_csv.reader(inp_file, delimiter=",")
    return csv, inp_file


def parseConfigRow(row):
    """
    Each row is of shape (len=8)
    location, bedname, bedmac, beltname, beltmac, leftwristname, leftwristmac, rightwristname, rightwristmac
    """

    location = row[0].strip()
    if (location.startswith(("AT", "BG", "SI", "DE", "XX"))) and (len(location) == 4):
        devs = {
            "bed": (row[1].strip(), row[2].strip()) if row[2].strip() else None,
            "belt": (row[3].strip(), row[4].strip()) if row[4].strip() else None,
            "wrist_l": (row[5].strip(), row[6].strip()) if row[6].strip() else None,
            "wrist_r": (row[7].strip(), row[8].strip()) if row[8].strip() else None,
            "bpm": row[9].strip() if row[9].strip() else None,
            "loc2": row[10].strip() if row[10].strip() else None,
            "comment": row[11].strip() if row[11].strip() else None,
            "production": row[12].strip() if row[12].strip() else None,
            "ecg": row[21].strip() if row[21].strip() else None,
            "ecg_pin": row[22].strip() if row[22].strip() else None,
        }

        return location, devs
    return None


def createConfig(configParams, locationInfo):
    import json as p_json

    loc_id, devices = locationInfo
    loc = {
        "location": "/etc/lgtc/loc-id",
        "mqtt_ip": configParams[2]
        if (devices["production"] == "YES")
        else configParams[8],
        "mqtt_port": int(configParams[3]),
        "mqtt_user": configParams[4],
        "mqtt_pwd": configParams[5],
        "topic": configParams[6],
        "mqtt_timestep": int(configParams[7]),
        "mqtt_on": True,
        "devices": [],
    }

    print(loc_id)
    if devices["bed"]:
        d = devices["bed"]
        loc["devices"].append(
            {
                "type": "microhub",
                "hubType": "BED",
                "mac": d[1],
                "location": "sens_bed",
                "comment": d[0],
            }
        )
    if devices["belt"]:
        d = devices["belt"]
        loc["devices"].append(
            {
                "type": "microhub",
                "hubType": "CLIP",
                "mac": d[1],
                "location": "sens_belt",
                "comment": d[0],
            }
        )
    if devices["wrist_l"]:
        d = devices["wrist_l"]
        loc["devices"].append(
            {
                "type": "microhub",
                "hubType": "WRIST",
                "mac": d[1],
                "location": "sens_bracelet_left",
                "comment": d[0],
            }
        )
    if devices["wrist_r"]:
        d = devices["wrist_r"]
        loc["devices"].append(
            {
                "type": "microhub",
                "hubType": "WRIST",
                "mac": d[1],
                "location": "sens_bracelet_right",
                "comment": d[0],
            }
        )
    if devices["bpm"]:
        loc["devices"].append(
            {
                "type": "bpm",
                "mac": devices["bpm"],
                "location": "sens_bpm",
                "comment": "BLE compatible BPM monitor",
            }
        )
    if devices["ecg"] and devices["ecg_pin"]:
        loc["devices"].append(
            {
                "type": "savvy",
                "mac": devices["ecg"],
                "pin": devices["ecg_pin"],
                "location": "sens_ecg",
                "comment": "Savvy!",
            }
        )
    with open(configParams[0] + "/" + loc_id + ".json", "w") as outfile:
        p_json.dump(loc, outfile, indent=2, sort_keys=True)


if __name__ == "__main__":
    import sys

    if len(sys.argv) < 2:
        print("Configuration file not given!")
        exit()

    config = open(sys.argv[1], "r").read().splitlines()

    if len(config) < 9:
        print("Configuration file doesn't have enough params")
        exit()

    # create directory for outputs
    import os

    if not os.path.exists(config[0].strip()):
        os.makedirs(config[0].strip())

    # get csv from gdrive
    csv_rows, _ = getDeploymentInfo(config[1].strip())

    # parse csv
    locations = []
    for row in csv_rows:
        loc = parseConfigRow(row)
        if loc:
            locations.append(loc)

    # outputs configs
    for location in locations:
        createConfig(config, location)