# Description of issue
Wrong behaviour noticed when analyzing data received on the SAAM database. When plotting data from the database there is a jump from 0G to 1G (expected is to start from 1G)

![Issue - plot](img/2020_09_07_wrist_issue.png)


# Issue
When reconnecting to the wristband first samples are either all 0s or are left from before disconnect.



Received samples after wristband restart.
```
[0.0, 0.0, 0.0] <<<
[0.0, 0.0, 0.0] <<<
[0.0, 0.0, 0.0] <<<
[0.0625, -0.2734375, 0.953125]
[0.046875, -0.2734375, 0.9609375]
[0.046875, -0.28125, 0.9453125]
[0.0625, -0.2890625, 0.9453125]
[0.046875, -0.2734375, 0.953125]
[0.0625, -0.2734375, 0.9453125]
[0.0546875, -0.2734375, 0.953125]

[0.0546875, -0.2734375, 0.953125]
[0.046875, -0.2890625, 0.953125]
[0.046875, -0.2734375, 0.9609375]
[0.0625, -0.2578125, 0.9453125]
...
```

Received samples after wristband reconnect (while disconnected, z axis on wristband was changed).
```
[0.0625, -0.28125, 0.953125]  <<<
[0.0625, -0.28125, 0.953125]  <<<
[0.0078125, -0.0078125, -1.0234375] 
[0.0078125, 0.0, -1.015625]
[0.015625, 0.0, -1.0234375]
[0.0078125, 0.0, -1.03125]
[0.015625, 0.0, -1.03125]
[0.0, -0.0078125, -1.0234375]
[0.0, 0.0, -1.0078125]
[0.015625, 0.0, -1.0234375]

[0.015625, 0.0, -1.015625]
[0.015625, -0.0078125, -1.015625]
[0.0078125, 0.0, -1.03125]
[0.0078125, -0.0078125, -1.0234375]
...
```

# Proposed solution
Throw away first n samples (first packet?) after reconnect. 

Fix: commit on 2020_09_08 ~9:30