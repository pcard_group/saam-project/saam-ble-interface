if __name__ == "__main__":
    import os, sys

    sys.path.append(
        os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))
    )

from math import pow
from struct import unpack
from COMMON_abst.device import Device, DeviceQueue
from COMMON_abst.logger import get_logger as saam_get_log

import time

nan = float('nan')

_UUID_BPM_CHAR = "00002a35-0000-1000-8000-00805f9b34fb"


def _iso_11073unpack(data) -> float:

    if (
        (data == 0x07FF)
        or (data == 0x0800)
        or (data == 0x7FE)
        or (data == 0x0802)
        or (data == 0x0801)
    ):
        return nan

    def _exp_helper(b) -> int:
        b = (b >> 12) & 0x000F
        if b >= 0x04:  # if negative create complement
            return -((0x000F + 1) - b)
        return b

    def _mantissa_helper(man) -> int:
        man = man & 0x0FFF
        if man >= 0x0800:  # if negative create complement
            return -((0x0FFF + 1) - man)
        return man

    exponent = _exp_helper(data)
    mantissa = _mantissa_helper(data)

    return mantissa * pow(10.0, exponent)


class BPMonitor(Device):
    @staticmethod
    def getDeviceType():
        return "bpm"

    def __init__(
        self, deviceMac: str, outputQueue: DeviceQueue = None, iface=None, name=None
    ):
        dev_log_name = "BPM_" + deviceMac
        logger = saam_get_log(dev_log_name, 1)
        Device.__init__(
            self,
            dev_log_name,
            "bpm",
            deviceMac,
            outputQueue,
            iface=iface,
            logger=logger,
        )

    def handleNotification(self, cHandle, data):
        # print("notification received!:", cHandle, "\n", data)
        # self._log("notification received!: " + str(cHandle) + "\n " + str(data))
        # FLAGS - 8bit
        flags = data[0]
        unit = "mmHg" if ((flags & 0x01) == 0) else "kPa"
        flag_has_timestamp = (flags & 0x02) > 0  # should be true
        flag_has_pulse = (flags & 0x04) > 0  # should be true
        flag_has_user_id = (flags & 0x08) > 0  # should be true
        flag_has_status = (flags & 0x10) > 0  # should be true

        systolic = _iso_11073unpack(unpack("H", data[1:3])[0])
        diastolic = _iso_11073unpack(unpack("H", data[3:5])[0])
        mean = _iso_11073unpack(unpack("H", data[5:7])[0])

        year = month = day = hour = minute = seconds = nan
        if flag_has_timestamp and (len(data) >= 14):
            year = unpack("H", data[7:9])[0]
            month = data[9]  # jan = 1
            day = data[10]
            hour = data[11]
            minute = data[12]
            seconds = data[13]

        pulse = nan
        if flag_has_pulse and (len(data) >= 16):
            pulse = _iso_11073unpack(unpack("H", data[14:16])[0])

        user_id = "unknown"
        if flag_has_user_id and (len(data) >= 17):
            user_id = data[16]
            if user_id == 0xFF:
                user_id = "unknown"

        status = ""
        if flag_has_status and (len(data) >= 19):
            status = "OK\n" if ((data[17] == 0) and (data[18] == 0)) else ""
            if data[17] & 0x01:
                status = status + "Body movement detected\n"
            if data[17] & 0x02:
                status = status + "Cuff fit issue detected\n"
            if data[17] & 0x04:
                status = status + "Irregular pulse detected\n"
            if data[17] & 0x08:
                status = status + "Pulse rate our of range\n"
            if data[17] & 0x10:
                status = status + "Measurement position\n"
            if data[17] & 0x20:
                status = status + "Device support multiple bonds\n"
            # bits 6-15 are reserved for future use

        parsed = {
            "u_id": user_id,
            "date": (year, month, day),
            "time": (hour, minute, seconds),
            "m_unit": unit,
            "m_systolic": systolic,
            "m_diastolic": diastolic,
            "m_bpm": pulse,
            "status": status,
        }
        self._log(str(parsed))
        self._sendToOutputDev(parsed)

    def state_machine_step(self, step_number):
        if not self.is_connected:
            return 0

        if step_number == 0:
            services = self.peripheral.getServices()
            characteristics = self.peripheral.getCharacteristics()
            descriptors = self.peripheral.getDescriptors()

            # get characteristic
            char_BP = self.peripheral.getCharacteristics(uuid=_UUID_BPM_CHAR)[0]
            # get descriptor of characteristic
            desc_BP = (char_BP.getDescriptors())[0]
            # enable indications
            desc_BP.write(b"\x02\x00", True)

            return 1

        self.peripheral.waitForNotifications(1)
        return 1


if __name__ == "__main__":

    # print(_iso_11073unpack(117))  # should return 117
    test_device = "28:fd:80:08:31:ad"
    bpm = BPMonitor(test_device, None, None, "test_device")

    bpm.startDevice()
    time.sleep(15)